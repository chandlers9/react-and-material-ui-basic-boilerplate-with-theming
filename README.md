**React and Material UI Boilerplate**

A barebones boilerplate w/create-react-app and Material UI using a sensible filing system.

I couldn't find just a BASIC and SIMPLE boilerplate which could be pulled, be easy to understand and could get me going FAST without a load of extra bells and whistles not relevant to my project. So here is what I made instead.

**How it was made**


1. Create-react-app

2. Installed Material UI Core

3. Deleted all the unneccessary/not useful UI stuff that comes with create-react-app

4. Restructured the file directories in accordance with this lecture https://vimeo.com/168648012 by Max Stoiber.
-- NB. This is how my boilerplate is intended to be used and Max Stoiber does provide a more complex, fully featured boilerplate if you want something more advanced to start off with. The reason for creating a new one is that not all websites neccessarily need all of the features and optimisations he talks about (e.g. my portfolio website which is essentially static)

5. Created a basic app structure w/ (nearly empty) Material UI Theming set up, responsive font resizing and a home page full of lorem ipsum.

**Hope you enjoy!**