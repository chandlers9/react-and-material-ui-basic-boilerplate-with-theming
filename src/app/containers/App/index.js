/* This is the container of the rest of the app -- handles navigation mainly */

import React from "react";
import { ThemeProvider } from "@material-ui/core";
import theme from "../../../themes/first";
/* Import pages  ----- Code splitting can be implemented here when have
                         multiple pages + navigation
                        to get significant performance improvements
*/
import Home from "../Home";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <header className="App-header">
          <Home />
        </header>
      </div>
    </ThemeProvider>
  );
}

export default App;
