/* A fairly empty theme which changes the primary and secondary coulour pallette
and also implements the responsiveFontSizes component to quickly and easily re-size text 
*/

import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";
import green from "@material-ui/core/colors/green";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: purple[500],
    },
    secondary: {
      main: green[500],
    },
  },
});

export default responsiveFontSizes(theme);
